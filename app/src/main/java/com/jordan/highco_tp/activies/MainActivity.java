package com.jordan.highco_tp.activies;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.jordan.highco_tp.R;
import com.jordan.highco_tp.database.DataBase;
import com.jordan.highco_tp.factory.ImageFactory;
import com.jordan.highco_tp.fragments.GridResultFragment;
import com.jordan.highco_tp.interfaces.IGoogleCustomSearch;
import com.jordan.highco_tp.models.Image;
import com.jordan.highco_tp.models.ImageDataSource;
import com.jordan.highco_tp.models.ImageRepository;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {

    private DataBase mDatabase;
    private ImageRepository mImageRepository;
    private ImageFactory mImageFactory = new ImageFactory();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Init Database
        mDatabase = DataBase.getInstance(this);
        mImageRepository = ImageRepository.getInstance(ImageDataSource.getInstance(mDatabase.imageDAO()));

        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView)searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d(this.getClass().toString(), "Research : " + query);

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(IGoogleCustomSearch.URL)
                        .build();

                IGoogleCustomSearch customSearch = retrofit.create(IGoogleCustomSearch.class);

                Call call = customSearch.customSearch(query,
                        "AIzaSyB77jTozHZ-SxEpYg4VTXdrI-iC2_reRlo",
                        "007637791949964062854:s6r1ffpdyay",
                        "photo",
                        "image",
                        "items(link)");
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {

                        new Thread(new Runnable() {
                            public void run() {
                                try {
                                    // CLEAR DATABASE
                                    mImageRepository.deleteAllImages();

                                    String result = response.body().string();
                                    JSONParser parser = new JSONParser();
                                    JSONObject j_object = (JSONObject) parser.parse(result);
                                    JSONArray j_array = (JSONArray) j_object.get("items");
                                    Iterator it = j_array.iterator();

                                    List<Image> tmpImageList = new ArrayList<>();
                                    while (it.hasNext()) {
                                        JSONObject item = (JSONObject) it.next();
                                        String urlImage = (String) item.get("link");

                                        // ADD NEW IMAGE IN DATABASE
                                        tmpImageList.add(mImageFactory.createImage(urlImage));
                                    }
                                    mImageRepository.insertImages(tmpImageList);

                                    // NOTIFY DATASOURCE CHANGE
                                    Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.gridResult);
                                    if(fragment instanceof GridResultFragment)
                                        ((GridResultFragment) fragment).updateRecyclerView();

                                }catch(Exception e)
                                {
                                    Log.e(this.getClass().toString(), "Exception to parse response to JSON OBJECT");
                                }
                            }
                        }).start();



                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("TAG", "onFailure: "+t.toString() );
                        // Log error here since request failed
                    }
                });
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }
}
