package com.jordan.highco_tp.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.jordan.highco_tp.interfaces.IImageDAO;
import com.jordan.highco_tp.models.Image;

import static com.jordan.highco_tp.database.DataBase.DATABASE_VERSION;

@Database(entities = Image.class, version = DATABASE_VERSION)
public abstract class DataBase extends RoomDatabase {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "DataBase";

    public abstract IImageDAO imageDAO();

    private static DataBase Instance;

    public static DataBase getInstance(Context context)
    {
        if(Instance == null)
        {
            Instance = Room.databaseBuilder(context, DataBase.class, DATABASE_NAME)
                    .fallbackToDestructiveMigration()
                    .build();
        }

        return Instance;
    }
}
