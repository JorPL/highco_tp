package com.jordan.highco_tp.models;

import com.jordan.highco_tp.interfaces.IImageDataSource;

import java.util.List;

public class ImageRepository implements IImageDataSource {

    private IImageDataSource mLocalDataSource;

    private static ImageRepository mInstance;

    public ImageRepository(IImageDataSource localDataSource) {
        this.mLocalDataSource = localDataSource;
    }

    public static ImageRepository getInstance(IImageDataSource localDataSource)
    {
        if(mInstance == null)
        {
            mInstance = new ImageRepository(localDataSource);
        }
        return mInstance;
    }

    @Override
    public void insertImage(Image image) {
        mLocalDataSource.insertImage(image);
    }

    @Override
    public void insertImages(List<Image> images) {
        mLocalDataSource.insertImages(images);
    }

    @Override
    public Image getImageById(int id) {
        return mLocalDataSource.getImageById(id);
    }

    @Override
    public List<Image> getAllImages() {
        return mLocalDataSource.getAllImages();
    }

    @Override
    public void updateImage(Image image) {
        mLocalDataSource.updateImage(image);
    }

    @Override
    public void deleteImage(Image image) {
        mLocalDataSource.deleteImage(image);
    }

    @Override
    public void deleteAllImages() {
        mLocalDataSource.deleteAllImages();
    }
}
