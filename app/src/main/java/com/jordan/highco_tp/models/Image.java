package com.jordan.highco_tp.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity
public class Image {

    @NonNull
    @PrimaryKey(autoGenerate = true)
    public int id;
    public String url;

    public Image(String url){
        setUrl(url);
    }

    public int getId() { return id; }
    public void setId(@NonNull int id) { this.id = id; }
    public String getUrl() { return url; }
    public void setUrl(String url) { this.url = url; }
}
