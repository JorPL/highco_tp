package com.jordan.highco_tp.models;

import com.jordan.highco_tp.interfaces.IImageDAO;
import com.jordan.highco_tp.interfaces.IImageDataSource;

import java.util.List;

public class ImageDataSource implements IImageDataSource {

    private IImageDAO mImageDAO;
    private static ImageDataSource mInstance;

    public ImageDataSource(IImageDAO imageDAO) {
        this.mImageDAO = imageDAO;
    }

    public static ImageDataSource getInstance(IImageDAO imageDAO)
    {
        if(mInstance == null)
        {
            mInstance = new ImageDataSource(imageDAO);
        }
        return mInstance;
    }

    @Override
    public void insertImage(Image image) {
        mImageDAO.insertImage(image);
    }

    @Override
    public void insertImages(List<Image> images) {
        mImageDAO.insertImages(images);
    }

    @Override
    public Image getImageById(int id) {
        return mImageDAO.getImageById(id);
    }

    @Override
    public List<Image> getAllImages() {
        return mImageDAO.getAllImages();
    }

    @Override
    public void updateImage(Image image) {
        mImageDAO.updateImage(image);
    }

    @Override
    public void deleteImage(Image image) {
        mImageDAO.deleteImage(image);
    }

    @Override
    public void deleteAllImages() {
        mImageDAO.deleteAllImages();
    }
}
