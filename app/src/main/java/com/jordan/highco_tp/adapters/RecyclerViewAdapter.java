package com.jordan.highco_tp.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.jordan.highco_tp.activies.FullScreenActivity;
import com.jordan.highco_tp.R;
import com.jordan.highco_tp.models.Image;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    List<Image> list;
    Context context;


    public RecyclerViewAdapter(List<Image> list, Context context) {
        this.list = list;
        this.context = context;
    }

    public void updateList(List<Image> list){
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_image,viewGroup,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        String url = list.get(position).getUrl();
        viewHolder.bindView(url);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView mImageView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.imageView);
            itemView.setOnClickListener(this);
        }

        public void bindView(String url){
            Glide.with(context).load(url).into(mImageView);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context,
                    FullScreenActivity.class);

            intent.putExtra("url", list.get(this.getAdapterPosition()).getUrl());
            context.startActivity(intent);
        }
    }
}
