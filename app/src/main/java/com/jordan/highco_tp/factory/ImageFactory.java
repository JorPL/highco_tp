package com.jordan.highco_tp.factory;

import com.jordan.highco_tp.models.Image;

public class ImageFactory {

    public Image createImage(String url){
        return new Image(url);
    }
}
