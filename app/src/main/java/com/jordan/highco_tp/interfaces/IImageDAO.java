package com.jordan.highco_tp.interfaces;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.jordan.highco_tp.models.Image;

import java.util.List;

@Dao
public interface IImageDAO {

    @Insert
    void insertImage(Image image);

    @Insert
    void insertImages(List<Image> images);

    @Query("SELECT * FROM Image WHERE id = :id")
    Image getImageById (int id);

    @Query("SELECT * FROM Image")
    List<Image> getAllImages();

    @Query("DELETE FROM Image")
    void deleteAllImages();

    @Update
    void updateImage (Image image);

    @Delete
    void deleteImage (Image image);


}
