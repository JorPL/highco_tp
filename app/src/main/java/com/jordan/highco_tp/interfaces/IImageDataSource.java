package com.jordan.highco_tp.interfaces;

import com.jordan.highco_tp.models.Image;

import java.util.List;

public interface IImageDataSource {

    void insertImage(Image image);

    void insertImages(List<Image> images);

    Image getImageById (int id);

    List<Image> getAllImages();

    void updateImage (Image image);

    void deleteImage (Image image);

    void deleteAllImages();
}
