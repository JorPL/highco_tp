package com.jordan.highco_tp.interfaces;


import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IGoogleCustomSearch {

    static final String URL = "https://www.googleapis.com";

    @GET("/customsearch/v1")
    Call<ResponseBody> customSearch(@Query("q") String query, @Query("key") String key, @Query("cx") String cx, @Query("imgType") String imgType, @Query("searchType") String searchType, @Query("fields") String fields);
}
