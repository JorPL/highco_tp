package com.jordan.highco_tp.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jordan.highco_tp.database.DataBase;
import com.jordan.highco_tp.models.ImageDataSource;
import com.jordan.highco_tp.models.ImageRepository;
import com.jordan.highco_tp.models.Image;
import com.jordan.highco_tp.R;
import com.jordan.highco_tp.adapters.RecyclerViewAdapter;

import java.util.ArrayList;
import java.util.List;

public class GridResultFragment extends Fragment {

    private RecyclerViewAdapter mAdapter;
    private ImageRepository mImageRepository;

    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_grid_image, container, false);

        // Initialisation de la base de données
        DataBase database = DataBase.getInstance(getContext());
        mImageRepository = ImageRepository.getInstance(ImageDataSource.getInstance(database.imageDAO()));

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        mAdapter = new RecyclerViewAdapter(new ArrayList<Image>(), getContext());
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(layoutManager);

        return view;
    }

    public void updateRecyclerView(){
        new Thread(new Runnable() {
            public void run() {
                try {
                    final List<Image> list = mImageRepository.getAllImages();

                    getActivity().runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            mAdapter.updateList(list);

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
